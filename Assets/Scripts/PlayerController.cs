﻿using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [Header("3D objects")]
    [SerializeField] private GameObject coin;
    [SerializeField] private Transform[] spawnPositions;
    [SerializeField] private TextMeshPro text;

    [Header("Parameters")]
    [SerializeField] private float speed;

    private int counter = 0;

    void Start()
    {
        text.text = "Coins : " + counter;
        Spawn();
    }

    void Update()
    {
        transform.Translate(Input.GetAxis("Horizontal") * speed * Time.deltaTime, 0, Input.GetAxis("Vertical") * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Coin")
        {
            counter++;
            text.text = "Coins : " + counter;

            Destroy(other.gameObject);
            Spawn();
        }
    }

    private void Spawn()
    {
        Instantiate(coin, spawnPositions[Random.Range(0, spawnPositions.Length)].position, coin.transform.rotation);
    }
}
