﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {

    private Transform player;
    private NavMeshAgent agent;

	void Start () {
        agent = GetComponent<NavMeshAgent>();
        player = FindObjectOfType<PlayerController>().transform;
	}
	
	void Update () {
        agent.SetDestination(player.position);
	}
}
