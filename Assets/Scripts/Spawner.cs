﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject enemy;
    public Transform[] enemyPositions;

	void Start () {
        EnemySpawn();
	}
	
	void Update () {
	}

    private void EnemySpawn() {
        Instantiate(enemy, enemyPositions[Random.Range(0, enemyPositions.Length)].position, enemy.transform.rotation);
    }
}
